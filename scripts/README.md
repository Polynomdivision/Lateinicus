# Scripts
A few scripts to help with any task

## csv_vocab_to_mongo.py
Parses the vocabulary files in `$REPO_ROOT/data/vocab/`, converts the vocabulary items
into a format that fits the database model for vocabulary items (see `backend/db.md`) and
writes the data into the MongoDB database.

**NOTE**: Requires `pymongo` to be installed via pip

### Usage
`python csv_vocab_to_mongo.py <URI> <Database>`

- `URI`: The URI of the MongoDB instance
- `Database`: The name of the database in the MongoDB instance

## add_user.js
Asks the user about his data (username, password, class** and adds the user to the database.

**NOTE**: Requires `mongodb` to be installed via npm

**NOTE**: This script is written in JavaScript, so we can ensure that the same pbkdf2 function is used
to create the user and to hash the password later on.

### Usage
`node add_user.js <URI> <Database>`

- `URI`: The URI of the MongoDB instance
- `Database`: The name of the database in the MongoDB instance

## add_level.py
Asks about a new level to add

**NOTE**: Requires `mongodb` to be installed via npm

### Usage

- `URI`: The URI of the MongoDB instance
- `Database`: The name of the database in the MongoDB instance

## generate_test_levels.py
Generates 10 levels with names and a description. The name and the description are generated using
Markov Chains, while the Bee Movie script serves as data for the model.

**Note**: Requires `mongodb`, `markovify` to be installed via pip
**Note**: Requires the file `bee-movie.txt` to be in the same folder as the script

### Usage

- `URI`: The URI of the MongoDB instance
- `Database`: The name of the database in the MongoDB instance
