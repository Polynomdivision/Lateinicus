const crypto = require("crypto");
const readline = require("readline");
const process = require("process");

const MongoClient = require("mongodb").MongoClient;

if (process.argv.length < 4) {
    console.log("Not enough arguments");
    process.exit(1);
}

function log(msg) {
    console.log("[*]", msg);
}

(async () => {
    const client = await (new MongoClient(process.argv[2])).connect();
    const db = client.db(process.argv[3]);

    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });

    rl.question("Username? > ", username => {
        rl.question("Class ID? > ", classId => {
            rl.question("Password? > ", password => {
                rl.question("Password (repeat)? > ", async (repeat) => {
                    if (repeat !== password) {
                        console.log(`Passwords for user ${username} don't match`);
                        rl.close();
                    } else {
                        // Generate a salt
                        log("Hashing password");
                        const salt = crypto.randomBytes(20).toString("hex");
                        const hash = crypto.pbkdf2Sync(password, salt, 50000, 512, "sha512").toString("hex");

                        // console.log(`Username: ${username}`);
                        // console.log(`Salt: ${salt}`);
                        // console.log(`Hash: ${hash}`);

                        const user = {
                            username,
                            salt,
                            hash,
                            // TODO: Fix this
                            classId,
                            // END TODO
                            score: 0,
                            showWelcome: true,
                            uid: 1,

                            lastReview: {
                                correct: 0,
                                wrong: 0,
                            },

                            lastLevel: 0,
                            levels: [],
                            vocabMetadata: {},
                        };

                        log("Writing to database...");
                        try {
                            await db.collection("users").insertOne(user);
                        } catch (err) {
                            log("Error while writing to database");
                            log(err);
                        }

                        rl.close();
                        process.exit(0);
                    }
                });
            });
        });
    });
})();
