import csv
import sys
import os
import pymongo

def log(msg, err=False, tabs=0):
    if (not err):
        print("[*] " + "\t" * tabs + msg)
    else:
        print("[X] " + "\t" * tabs + msg)

def dbg(msg, tabs=0):
    print("[D] " + "\t" * tabs + msg)
   
def csv_to_level(filename):
    skip = 0
    path = os.path.join("../data/", filename)
    dbg("Reading from {0} ({1})".format(filename, path), tabs=2)
    queries = []
    with open(path, newline="") as csvfile:
        reader = csv.reader(csvfile, delimiter=",", quotechar="|")
        for raw in reader:
            skip += 1
            # Skip the header lines
            # if (skip < num_lines_to_skip + 1):
            if (skip < 3):
                continue

            # Remove any whitespaces in front of or after the string
            row = [col.strip() for col in raw]

            queries += (({
                "level": int(row[0])
            }, {
                "$set": {
                    "name": row[1],
                    "description": row[4]
                }
            }),)
            
    return queries

log("Lateinicus CSV to level DB Model")
if len(sys.argv) < 3 and os.getenv("DEBUG") == None:
    log("Not enough arguments!", err=True)
    log("Usage: csv_vocab_to_mongo.py <URI> <Database>", err=True)
    sys.exit(1)

log("Creating queries")
queries = csv_to_level("levels.csv")
if os.getenv("DEBUG") != None:
    log("{} entries generated".format(len(queries)))
    log("Query: {}".format(queries[0][0]))
    log("Update: {}".format(queries[0][1]))
    sys.exit()

# Connect to the database
log("Updating levels")
log("Connecting...", tabs=1)
client = pymongo.MongoClient(sys.argv[1])
log("Getting DB...", tabs=1)
db = client[sys.argv[2]]

index = 1
for query in queries:
    log("Updating [{0}/{1}]...".format(index, len(queries)), tabs=1)
    index += 1
    res = db["levels"].update_one(query[0], query[1])
