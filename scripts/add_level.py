import pymongo
import sys

def log(msg, err=False, tabs=0):
    if (not err):
        print("[*] " + "\t" * tabs + msg)
    else:
        print("[X] " + "\t" * tabs + msg)

def dbg(msg, tabs=0):
    print("[D] " + "\t" * tabs + msg)

if (len(sys.argv) < 3):
    log("Not enough arguments!", err=True)
    sys.exit(1)

log("Level")
level = int(input("> "))

log("Level name")
name = input("> ")

log("Level description")
desc = input("> ")

log("Vocabulary (Comma separated)")
vocab_raw = input("> ")

log("Preprocess vocabulary list")
vocab = []
for id_raw in vocab_raw.split(","):
    try:
        vocab.append(int(id_raw))
    except:
        log("Invalid id found: '{0}'".format(id), err=True)
        sys.exit(1)

log("Connecting to database...")    
client = pymongo.MongoClient(sys.argv[1])
log("Getting DB...")
db = client[sys.argv[2]]
log("Inserting...")
res = db["levels"].insert_one({
    "level": level,
    "name": name,
    "description": desc,
    "vocab": vocab
})

log("Success", tabs=1)
