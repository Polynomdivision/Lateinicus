# Lateinicus
Eine experimentelle, auf Latein spezialisierte Lernanwendung.

## Warum?
Zum Ermitteln, wie effektiv das Konzept der [Gamification](https://de.wikipedia.org/wiki/Gamification) ist, wird
diese experimentelle Lernanwendung verwendet.
