export enum TrackerEvent {
    LOG_IN = "LOG_IN",
    LOG_OUT = "LOG_OUT",
    START_LEARNING = "START_LEARNING",
    CANCEL_LEARNING = "CANCEL_LEARNING",
    FINISH_LEARNING = "FINISH_LEARNING",
};

export interface ITrackerRequest {
    session: string;
    event: TrackerEvent;
};

export type ITrackerDBModel = ITrackerRequest & {
    timestamp: number;
};
