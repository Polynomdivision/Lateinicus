# 05.09.2018
Sick

# 06.09.2018
- Implementing the view introducing vocabulary to the user

# 14.09.2018
- Implementing the Levenshtein Distance for future checking of answers

# 15.09.2018
- Implementing the review functionality of the application
- Created a intro page to tell the students about our project

# 16.09.2018
- Implement the SuperMemo2 algorithm
- Started implementing the server

# 18.09.2018
- Move the whole application from "vanilla React" to React with Redux to avoid issues with components
  being re-mounted, when the AppBar gets updated

# 29.09.2018
- Finish implementing the most needed API endpoints

# 01.10.2018
- Write scripts to...
	- automatically convert the vocabulary spreadsheets into database entries
	- create users
	- create levels 
# 02.10.2018
- Fix bugs in the WebUI
	- Not all vocabulary items get turned into a question
	- The review page does not show a loading spinner when fetching data
- Show tick marks when a user has completed a level

# 03.10.2018
- Secure the database

# 08.10.2018
- Set up a Droplet to host the software

# 09.10.2018
- Fix bugs:
	- Checking long words during the review crashes the browser tab
	- Some questions fail because of a whitespace at the end of the expected answer

# 10.10.2018
- Redesign the Level UI

# ???
- Register a domain
- Issue a Let's Encrypt certificate for HTTPS
- Fix issues in the "vocabulary-import-script"
	- Accidental whitespaces mess up the vocabulary check, so they should be filtered

# 16.10.2018
- Replace `alert`s with Material UI Dialogs

# 19.10.2018
- Fix a bug where the vocabulary metadata always falls back to `NaN`
- Remove the "Perfekt" from the vocabulary

# 21.10.2018
- Implement mnemonics
