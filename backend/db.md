# User
`collection`: users
`
{
    username: string,
    salt: string,
    hash: string,
    uid: number,
    showWelcome: boolean,
    classId: string,
    score: number,
    
    lastReview: {
       correct: number,
       wrong: number,
    },
   
    lastLevel: number,
    levels: number[], 
    
    vocabMetadata: {
        [id: number]: ISM2Metadata,
    },
}
`

- queue stores IDs of the vocabulary

# Vocab
`collection`: vocabulary
`
{
    id: string,
    german: string[],
    hint: string?,
    mnemonic: string?,
    
    type: VocabType,
    
    latin: INomenData | IVerbData | IAdjektivData (See frontend models),
`

- VocabType -> number?
- id == _id?

# Levels
`collection`: levels
`
{
    level: number;
    name: string;
    description: string;
    
    vocab: number[],
}
`

- vocab stores the IDs of the vocabulary items from the `vocabulary` collection

# Sessions
column: sessions

`
{
    username: string,
    session: string,
}
`

