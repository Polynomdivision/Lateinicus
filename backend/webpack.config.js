const path = require("path");

module.exports = (env, mode) => {
    const production = mode === "production";

    return {
        entry: "./src/main.ts",
        devtool: production ? "source-map": "inline-source-map",
        target: "node",
        
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    use: "ts-loader",
                    exclude: /node_modules/
                }
            ]
        },
        resolve: {
            extensions: [".ts", ".js"]
        },
        output: {
            filename: "bundle.js",
            path: path.resolve(__dirname, "dist")
        }
    };
};
