export interface ISchedulingData {
    easiness: number;
    consecutiveCorrectAnswers: number;
    nextDueDate: number;
};

function dateInNDays(n: number): number {
    //@ts-ignore
    return new Date(Date.now() + 1000 * 60 * 60 * 24 * n);
}

export enum AnswerType {
    CORRECT,
    WRONG,
};

function performanceRating(answer: AnswerType): number {
    switch (answer) {
        case AnswerType.WRONG:
            return 1;
        case AnswerType.CORRECT:
            return 4;
    }
}

export function updateSchedulingData(data: ISchedulingData, answer: AnswerType): ISchedulingData {
    const perfRating = performanceRating(answer);

    data.easiness += -0.8 + 0.28 * perfRating + 0.02 * Math.pow(perfRating, 2);
    data.consecutiveCorrectAnswers = answer === AnswerType.CORRECT ? (
        data.consecutiveCorrectAnswers + 1
    ) : 0;
    data.nextDueDate = answer === AnswerType.CORRECT ? (
        dateInNDays(6 * Math.pow(data.easiness, data.consecutiveCorrectAnswers - 1))
    ) : dateInNDays(1);

    return data;
}
