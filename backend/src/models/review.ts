export interface ISM2Metadata {
    easiness: number;
    consecutiveCorrectAnswers: number;
    nextDueDate: number;
};
