import { ISM2Metadata } from "./review";

export interface IUserDBModel {
    username: string;
    uid: string;
    showWelcome: boolean;
    score: number;
    classId: string;

    sessionToken: string;

    lastReview: {
        correct: number;
        wrong: number;
    };

    // Levels that we have done
    levels: number[];
    // The "highest" level the user has done
    lastLevel: number;

    queue: number[];

    // Vocabulary ID -> SM2 Metadata
    vocabMetadata: {
        [key: number]: ISM2Metadata;
    };
};

export interface IUser {
    username: string;
    uid: string;
    showWelcome: boolean;
    score: number;
    classId: string;

    sessionToken: string;
};
