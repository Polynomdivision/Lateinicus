import { Request, Response } from "express";

import { isAuthenticated } from "../security/auth";

export async function authRoute(req: Request, res: Response, next: () => void) {
    const token = req.get("Token");
    if (token) {
        // Check if were authenticated
        //@ts-ignore
        const auth = await isAuthenticated(token, req.db);
        if (auth) {
            //@ts-ignore
            req.token = token;
            next();
        } else {
            res.send({
                error: "403",
                data: {
                    msg: "Session Token not found!",
                },
            });
        }
    } else {
        res.send({
            error: "403",
            data: {
                msg: "No Session Token specified",
            },
        });
    }
};
