import { Request } from "express";

import { Db } from "mongodb";

export type LRequest = Request & {
    db: Db;
    token: string;
};
