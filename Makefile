.PHONY: frontend
production:
	@echo Building the frontend
	$(MAKE) -C frontend/ prod

	@echo Building the backend
	$(MAKE) -C backend/ prod

development:
	@echo Building the frontend
	$(MAKE) -C frontend/ dev

	@echo Building the backend
	$(MAKE) -C backend/ dev

.PHONY: todo
todo:
	ag TODO frontend/src/ backend/src/
