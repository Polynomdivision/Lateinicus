#!/bin/bash
set -x
# Build the App
./node_modules/.bin/parcel build

# Minify the JS
if [ ! -d ./dist/minified_js ]; then
    mkdir ./dist/minified_js
fi
for file in ./dist/*.js; do
    echo "== MINIFYING $file =="
    ./node_modules/.bin/uglifyjs --mangle --compress -o "./dist/minified_js/$(basename $file)" -- $file
done
