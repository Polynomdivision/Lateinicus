#!/bin/bash
rm -rf dist/

./node_modules/.bin/parcel build --out-dir dist/app src/index.html
./node_modules/.bin/parcel build --out-dir dist src/lost.html src/error.html
chmod 705 dist dist/app
chmod 604 dist/app/*

sed -e "s/\/src/\/app\/src/g" --in-place dist/app/index.html
