export enum ReviewMode {
    GER_TO_LAT,
    LAT_TO_GER,
};

export enum VocabType {
    NOMEN = 0,
    VERB = 1,
    ADJEKTIV = 2,
    ADVERB = 3,
};

export interface INomenData {
    grundform: string;
    genitiv: string;
    genus: string;
};

export interface IVerbData {
    grundform: string;
    // 1. Person
    praesens: string;
    perfekt: string;
};

export interface IAdjektivData {
    grundform: string;
    endung_f: string;
    endung_n: string;
};

export interface IAdverbData {
    grundform: string;
};

export interface IVocab {
    // If a word has multiple meanings
    german: string[];
    hint?: string;
    mnemonic?: string;

    type: VocabType;
    latin: INomenData | IVerbData | IAdjektivData | IAdverbData;

    // This number is unique across all vocabulary items
    id: number;
};

// What kind of question should be answered
export enum ReviewQType {
    GERMAN,

    NOMEN_GENITIV,
    NOMEN_GENUS,

    ADJ_ENDUNG_F,
    ADJ_ENDUNG_N,

    VERB_PRAESENS,
    VERB_PERFEKT,
};

export interface IReviewCard {
    question: string;
    // If a question can have multiple answers
    answers: string[];

    qtype: ReviewQType;

    // Identical to its corresponding IVocab item
    id: number;
};

export function reviewQTypeToStr(type: ReviewQType): string {
    switch (type) {
        case ReviewQType.GERMAN:
            return "Übersetzung";
        case ReviewQType.NOMEN_GENITIV:
            return "Genitiv";
        case ReviewQType.NOMEN_GENUS:
            return "Genus";
        case ReviewQType.ADJ_ENDUNG_F:
            return "Endung feminin";
        case ReviewQType.ADJ_ENDUNG_N:
            return "Endung neutrum";
        case ReviewQType.VERB_PRAESENS:
            return "1. Person Präsens";
        case ReviewQType.VERB_PERFEKT:
            return "1. Person Perfekt";
    }
}

// Turn a vocabulaty item into a series of questions about the item
export function vocabToReviewCard(vocab: IVocab): IReviewCard[] {
    switch (vocab.type) {
        case VocabType.NOMEN:
            const nData = vocab.latin as INomenData;
            return [{
                // Latin -> German
                question: nData.grundform,
                answers: vocab.german,
                qtype: ReviewQType.GERMAN,
                id: vocab.id,
            }, {
                // Latin -> Genitiv
                question: nData.grundform,
                answers: [nData.genitiv],
                qtype: ReviewQType.NOMEN_GENITIV,
                id: vocab.id,
            }, {
                // Latin -> Genus
                question: nData.grundform,
                answers: [nData.genus],
                qtype: ReviewQType.NOMEN_GENUS,
                id: vocab.id,
            }];
        case VocabType.VERB:
            const vData = vocab.latin as IVerbData;
            return [{
                // Latin -> German
                question: vData.grundform,
                answers: vocab.german,
                qtype: ReviewQType.GERMAN,
                id: vocab.id,
            }, {
                // Latin -> Präsens
                question: vData.grundform,
                answers: [vData.praesens],
                qtype: ReviewQType.VERB_PRAESENS,
                id: vocab.id,
            }];
        case VocabType.ADJEKTIV:
            const aData = vocab.latin as IAdjektivData;
            return [{
                // Latin -> German
                question: aData.grundform,
                answers: vocab.german,
                qtype: ReviewQType.GERMAN,
                id: vocab.id,
            }, {
                // Latin -> Endung f
                question: aData.grundform,
                answers: [aData.endung_f],
                qtype: ReviewQType.ADJ_ENDUNG_F,
                id: vocab.id,
            }, {
                // Latin -> Perfekt
                question: aData.grundform,
                answers: [aData.endung_n],
                qtype: ReviewQType.ADJ_ENDUNG_N,
                id: vocab.id,
            }];
        case VocabType.ADVERB:
            return [{
                // Latin -> German
                question: vocab.latin.grundform,
                answers: vocab.german,
                qtype: ReviewQType.GERMAN,
                id: vocab.id,
            }];
    }
}
