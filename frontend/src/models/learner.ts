export interface ILearner {
    username: string;
    score: number;
}

export type TopTen = ILearner & { nr: number };
