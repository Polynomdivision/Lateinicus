export interface ILevel {
    name: string;
    description: string;
    level: number;

    done: boolean;
}
