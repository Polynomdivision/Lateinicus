export interface IReviewMetadata {
    // Number of correct answers
    correct: number;
    // Number of wrong answers
    wrong: number;
};

export enum ReviewType {
    LEVEL,
    QUEUE
};
