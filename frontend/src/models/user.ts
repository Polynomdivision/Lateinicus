export interface IUser {
    username: string;
    uid: string;
    showWelcome: boolean;
    score: number;
    classId: string;

    // Levels that the user has completed
    levels: number[];

    sessionToken: string;
};

export interface IUserLevel {
    // The numerical representation
    level: number;
    // The string representation
    name: string;
    // The user has this level until: score => levelCap
    levelCap: number;
};

function levelFactory(): (name: string, levelCap: number) => IUserLevel {
    let level = 1;
    return (name: string, levelCap: number) => {
        return {
            level: level++,
            name,
            levelCap,
        };
    };
}
const l = levelFactory();

export const UserLevels: IUserLevel[] = [
    l("Sklave", 35),
    l("Farmer", 75),
    l("Soldat", 120),
    l("Gladiator", 170),
    l("Zenturio", 220),
    l("Prätor", 270),
    l("Reiter", 320),
    l("General", 370),
    l("Konsul", 420),
    l("Caesar", 470),
];

export function userScoreToLevel(userScore: number): IUserLevel {
    // NOTE: The failure level should never be returned
    return UserLevels.find((el) => userScore < el.levelCap) || UserLevels[UserLevels.length - 1];
}
