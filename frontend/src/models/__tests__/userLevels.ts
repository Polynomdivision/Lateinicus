import { userScoreToLevel } from "../user";

it("Should return level 1 for 34", () => {
    const level = userScoreToLevel(34);
    expect(level.level).toBe(1);
});

it("Should return level 2 for 35", () => {
    const level = userScoreToLevel(35);
    expect(level.level).toBe(2);
});
