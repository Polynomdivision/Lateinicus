export interface IResponse {
    error: string;
    data: any;
};
