import * as React from "react";

import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import SummaryTable from "../components/SummaryTable";

import { withRouter } from "react-router-dom";

import { IReviewMetadata } from "../models/review";

interface IProps {
    history: any;

    reviewMeta: IReviewMetadata;
    setDrawerButton: (state: boolean) => void;
}

// TODO: This stays at the default value
const SummaryPageWithRouter = withRouter(
    class SummaryPage extends React.Component<IProps> {
        toDashboard = () => {
            // Show the drawer button
            this.props.setDrawerButton(true);

            // Go to the dashboard
            this.props.history.push("/dashboard");
        }

        render() {
            return <div className="content">
                <Grid
                    container
                    spacing={0}
                    direction="column"
                    alignItems="center"
                    justify="center"
                    style={{ minHeight: '100vh' }}>
                    <Grid item xs={12}>
                        <Paper className="paper">
                            <Typography variant="title">Zusammenfassung</Typography>
                            <Grid container direction="column">
                                <SummaryTable reviewMeta={this.props.reviewMeta} />
                                <Button onClick={this.toDashboard}>
                                    Zum Dashboard
                            </Button>
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
            </div>;
        }
    }
);
export default SummaryPageWithRouter;
