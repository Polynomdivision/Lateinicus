import * as React from "react";

import { Link } from "react-router-dom";

import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";

interface IProps {
    dontShowAgain: () => void;
};

export default class IntroPage extends React.Component<IProps> {
    render() {
        const small = window.matchMedia("(max-width: 700px)").matches;
        const cName = small ? "intro-card-xs" : "intro-card-lg";
        return <div className="content">
            <Grid container justify="center">
                <Card className={cName}>
                    <CardContent>
                        <Typography
                            component="p"
                            variant="title">
                            Wilkommen bei Lateinicus!
                        </Typography>

                        <div className="vert-spacer" />
                        <Typography
                            className="intro-subheading"
                            component="p"
                            variant="subheading">
                            Was ist Lateinicus?
                        </Typography>

                        <Typography
                            variant="body1"
                            component="p">
                            Lateinicus ist eine experimentelle Lernanwendung für Lateinvokabeln. Mit Hilfe dieser
                            Anwendung wollen wir die Frage beantworten, ob "Gamification" tatsächlich effektiver
                            ist als klassisches Lernen mit Vokabelheft.
                        </Typography>
                        <Typography
                            variant="body1"
                            component="p">
                            Um die Effektivität zu bewerten wird vor und nach Verwendung von Lateinicus
                            ein Vokabeltest geschrieben, welche von uns zum Zwecke der Datenerhebung benutzt
                            werden.
                        </Typography>

                        <Typography
                            variant="body1"
                            component="p">
                            Zudem erfassen wir Daten darüber, wann und wie lange gelernt wird, damit wir diese
                            Werte auch in unser Fazit mit einfließen lassen können.
                        </Typography>

                        <div className="vert-spacer" />
                        <Typography
                            className="intro-subheading"
                            component="p"
                            variant="subheading">
                            Wie funktioniert Lateinicus?
                        </Typography>

                        <Typography
                            variant="body1"
                            component="p">
                            Wir haben die zu lernenden Vokabeln in verschiedene kleinere Levels unterteilt. Dabei erhälst du Punkte für das
                            absolvieren von Levels oder Wiederholungen.
                        </Typography>
                        <Typography
                            variant="body1"
                            component="p">
                            Zudem gibt es auch Nutzerlevels: So startet ihr mit Level 1 (Sklave) und könnt euch bis zu Level 10 (Caesar) hocharbeiten.
                        </Typography>


                        <div className="vert-spacer" />
                        <Typography
                            className="intro-subheading"
                            component="p"
                            variant="subheading">
                            Was ist Gamification?
                        </Typography>

                        <Typography
                            variant="body1">
                            Gamification ist ein Konzept, bei welchem eine Tätigkeit, wie hier das Lernen von
                            Lateinvokabeln, in ein Spiel umgewandelt wird.
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button
                            fullWidth={true}
                            component={Link}
                            to="/dashboard"
                            onClick={this.props.dontShowAgain}>
                            Lass uns loslegen
                        </Button>
                    </CardActions>
                </Card>
            </Grid>
        </div>;
    }
};
