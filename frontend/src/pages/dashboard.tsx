import * as React from "react";

import { Link } from "react-router-dom";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";

import Scoreboard from "../components/scoreboard";
import SummaryTable from "../components/SummaryTable";
import Loader from "../components/loading";

import { ILevel } from "../models/level";
import { TopTen } from "../models/learner";
import { IReviewMetadata } from "../models/review";

interface IProps {
    getDashboard: () => Promise<any>;

    loading: boolean;
    setLoading: (state: boolean) => void;
    nextLevel: ILevel;
    setNextLevel: (level: ILevel) => void;
    topTen: TopTen[];
    setTopTen: (topten: TopTen[]) => void;
    lastReview: IReviewMetadata;
    setLastReview: (review: IReviewMetadata) => void;
}

export default class Dashboard extends React.Component<IProps> {
    componentDidMount() {
        this.props.setLoading(true);
        this.props.getDashboard().then(res => {
            this.props.setNextLevel(res.nextLevel);
            this.props.setTopTen(res.topTen);
            this.props.setLastReview(res.lastReview);
            this.props.setLoading(false);
        })
    }

    render() {
        if (this.props.loading) {
            return <Loader />;
        }

        const small = window.matchMedia("(max-width: 700px)").matches;
        const direction = small ? "column" : "row";

        const level = this.props.nextLevel;

        return <div className="content">
            <Grid container direction={direction} spacing={16}>
                <Grid item lg={4}>
                    <Paper className="paper">
                        <div>
                            <Typography variant="title">{`Level ${level.level}`}</Typography>
                            <Typography variant="title" component="p">{level.name}</Typography>
                            <br />
                            <Typography component="p">
                                {level.description}
                            </Typography>
                            <Button
                                component={Link}
                                to={`/level/${level.level}`}
                                className="lesson-card-btn">
                                Zum Level
                            </Button>
                        </div>
                    </Paper>
                </Grid>
                <Grid item lg={4}>
                    <Paper className="paper">
                        <div>
                            <Typography variant="title" component="p">
                                Rangliste: Top 10
                            </Typography>

                            <Scoreboard topTen={this.props.topTen} />
                        </div>
                    </Paper>
                </Grid>
                <Grid item lg={4}>
                    <Paper className="paper">
                        <div>
                            <Typography variant="title">
                                Letzte Wiederholung
                            </Typography>
                            <SummaryTable reviewMeta={this.props.lastReview} />

                            <Button
                                component={Link}
                                to="/review/queue"
                                className="lesson-card-btn">
                                Vokabeln üben
                            </Button>
                        </div>
                    </Paper>
                </Grid>
            </Grid>
        </div>;
    }
};
