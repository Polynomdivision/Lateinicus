import * as React from "react";

import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Snackbar from "@material-ui/core/Snackbar";

import Loader from "../components/loading";

import { withRouter } from "react-router-dom";

import { ILevel } from "../models/level";
import { IUser } from "../models/user";

interface IProps {
    getLevels: () => Promise<any>;

    history: any;

    user: IUser;
    setLevels: (levels: ILevel[]) => void;
    setLoading: (state: boolean) => void;
    loading: boolean;
    snackbar: boolean;
    setSnackbar: (state: boolean) => void;
    levels: ILevel[];
}

const LevelListWithRouter = withRouter(
    class Dashboard extends React.Component<IProps> {
        componentDidMount() {
            this.props.setLoading(true);

            // Fetch the levels
            this.props.getLevels().then(res => {
                this.props.setLevels(res.levels);
                this.props.setLoading(false);
            });
        }

        toLevel(id: number) {
            const maxLevel = Math.max(...this.props.user.levels);
            if ((maxLevel === -Infinity || maxLevel === +Infinity) || maxLevel + 1 >= id) {
                this.props.history.push(`/level/${id}`);
            } else {
                this.props.setSnackbar(true);
            }
        }

        render() {
            if (this.props.loading) {
                return <Loader />;
            }

            const small = window.matchMedia("(max-width: 700px)").matches;
            const cName = small ? "lesson-card-xs" : "lesson-card-lg";
            const { levels } = this.props.user;

            let key = 0;
            const levelToCard = (level: ILevel) => {
                const suffix = levels.indexOf(level.level) !== -1 ? " ✔" : "";
                return <Grid item key={key++}>
                    <Card style={{
                        width: small ? window.innerWidth - 32 : "300px"
                    }}>
                        <CardContent className={cName}>
                            <Typography variant="title">{`Level ${level.level}${suffix}`}</Typography>
                            <Typography variant="title" component="p">{level.name}</Typography>
                            <br />
                            <Typography component="p">
                                {level.description}
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <Button
                                className="lesson-card-btn"
                                onClick={() => this.toLevel(level.level)}>
                                Zum Level
                        </Button>
                        </CardActions>
                    </Card>
                </Grid>;
            };

            return <div className="content">
                <Grid container spacing={16} direction="row">
                    {this.props.levels.map(levelToCard)}
                </Grid>
                <Snackbar
                    anchorOrigin={{
                        vertical: "top",
                        horizontal: "right",
                    }}
                    open={this.props.snackbar}
                    onClose={() => this.props.setSnackbar(false)}
                    message={<span>Du hast dieses Level noch nicht freigeschaltet!</span>} />
            </div>;
        }
    }
);
export default LevelListWithRouter;
