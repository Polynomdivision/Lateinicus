import { connect } from "react-redux";

import {
    setLevelListLoading, setLevels, setLevelListSnackbar
} from "../actions";

import { ILevel } from "../models/level";

import LevelListPage from "../pages/levelList";

const mapStateToProps = state => {
    return {
        levels: state.levels,
        loading: state.levelList.loading,
        user: state.user,
        snackbar: state.levelList.snackbar,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        setLoading: (state: boolean) => dispatch(setLevelListLoading(state)),
        setLevels: (levels: ILevel[]) => dispatch(setLevels(levels)),
        setSnackbar: (state: boolean) => dispatch(setLevelListSnackbar(state)),
    };
};

const LevelListContainer = connect(mapStateToProps,
    mapDispatchToProps)(LevelListPage);
export default LevelListContainer;
