import { connect } from "react-redux";

import { setLoginSnackbar, setLoginLoading } from "../actions";

import LoginPage from "../pages/login";

const mapStateToProps = state => {
    return {
        loading: state.login.loading,
        snackOpen: state.login.snackOpen,
        snackMsg: state.login.snackMsg,
        authenticated: state.authenticated,
        didLogin: state.didLogin,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        setLoading: (state: boolean) => dispatch(setLoginLoading(state)),
        setSnackbar: (state: boolean, msg: string) => dispatch(setLoginSnackbar(state, msg)),
    };
};

const LoginPageContainer = connect(mapStateToProps, mapDispatchToProps)(LoginPage);
export default LoginPageContainer;
