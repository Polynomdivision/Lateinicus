import { connect } from "react-redux";

import {
    setNextLevel, setTopTen, setLastReview, setDashboardLoading
} from "../actions";

import { ILevel } from "../models/level";
import { ILearner } from "../models/learner";
import { IReviewMetadata } from "../models/review";

import DashboardPage from "../pages/dashboard";

const mapStateToProps = state => {
    return {
        loading: state.review.loading,
        nextLevel: state.nextLevel,
        topTen: state.topTen,
        lastReview: state.lastReview,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        setLoading: (state: boolean) => dispatch(setDashboardLoading(state)),
        setNextLevel: (level: ILevel) => dispatch(setNextLevel(level)),
        setTopTen: (topTen: ILearner[]) => dispatch(setTopTen(topTen)),
        setLastReview: (review: IReviewMetadata) => dispatch(setLastReview(review)),
    }
};

const DashboardContainer = connect(mapStateToProps,
    mapDispatchToProps)(DashboardPage);
export default DashboardContainer;
