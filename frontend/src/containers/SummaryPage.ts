import { connect } from "react-redux";

import { setDrawerButton } from "../actions";

import SummaryPage from "../pages/summary";

const mapStateToProps = state => {
    return {
        reviewMeta: state.lastReview,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setDrawerButton: (state: boolean) => dispatch(setDrawerButton(state)),
    };
};

const SummaryPageContainer = connect(mapStateToProps,
    mapDispatchToProps)(SummaryPage);
export default SummaryPageContainer;
