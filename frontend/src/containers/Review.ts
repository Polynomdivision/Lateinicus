import { connect } from "react-redux";

import {
    setDrawerButton, setReviewPopover, setReviewSummary,
    setReview, setReviewLoading, setReviewDialog, setReviewHelp,
    setReviewModal
} from "../actions";

import { IReviewMetadata } from "../models/review";
import { IVocab } from "../models/vocab";
import ReviewPage from "../pages/review";

const mapStateToProps = state => {
    return {
        dialogOpen: state.review.dialogOpen,
        metadata: state.review.metadata,
        vocab: state.review.vocab,
        current: state.review.current,
        popoverOpen: state.review.popoverOpen,
        popoverText: state.review.popoverText,
        popoverColor: state.review.popoverColor,
        popoverTextColor: state.review.popoverTextColor,
        loading: state.review.loading,
        showHelp: state.review.showHelp,

        modalShow: state.review.modalShow,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        drawerButtonState: (state: boolean) => dispatch(setDrawerButton(state)),
        setPopover: (state: boolean, text: string, color: string, textColor: string) => dispatch(setReviewPopover(state, text, color, textColor)),
        setSummary: (state: boolean) => dispatch(setReviewSummary(state)),
        setReview: (current: IVocab, meta: IReviewMetadata) => dispatch(setReview(current, meta)),
        setLoading: (state: boolean) => dispatch(setReviewLoading(state)),
        setReviewDialog: (state: boolean) => dispatch(setReviewDialog(state)),
        setShowHelp: (state: boolean) => dispatch(setReviewHelp(state)),

        setModal: (state: boolean) => dispatch(setReviewModal(state)),
    };
};

const ReviewContainer = connect(mapStateToProps,
    mapDispatchToProps)(ReviewPage);
export default ReviewContainer;
