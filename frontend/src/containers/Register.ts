import { connect } from "react-redux";

import {
    setRegisterSnackbar, setRegisterLoading
} from "../actions";

import RegisterPage from "../pages/register";

const mapStateToProps = state => {
    return {
        loading: state.register.loading,
        snackOpen: state.register.snackOpen,
        snackMsg: state.register.snackMsg,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        setLoading: (state: boolean) => dispatch(setRegisterLoading(state)),
        setSnackbar: (state: boolean, msg: string) => dispatch(setRegisterSnackbar(state, msg)),
    }
};

const RegisterContainer = connect(mapStateToProps,
    mapDispatchToProps)(RegisterPage);
export default RegisterContainer;
