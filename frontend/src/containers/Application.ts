import { connect } from "react-redux";

import { IUser } from "../models/user";

import Application from "../components/app";

import {
    setAuthenticated, setUser, setDidLogin, setLastReview,
    setUserScoreDelta
} from "../actions";

import { IReviewMetadata } from "../models/review";

const mapStateToProps = state => {
    return {
        authenticated: state.authenticated,
        user: state.user,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        setAuthenticated: (status: boolean) => dispatch(setAuthenticated(status)),
        setDidLogin: (state: boolean) => dispatch(setDidLogin(state)),
        setUser: (user: IUser) => dispatch(setUser(user)),
        setLastReview: (meta: IReviewMetadata) => dispatch(setLastReview(meta)),
        setUserScoreDelta: (delta: number) => dispatch(setUserScoreDelta(delta)),
    };
};

const ApplicationContainer = connect(mapStateToProps,
    mapDispatchToProps)(Application);

export default ApplicationContainer;
