import { connect } from "react-redux";

import {
    setDrawerButton, setLevelLookedAt, setLevelStepper, setLevelReviewDiag,
    setLevelCurrentVocab, setLevelVocab, setLevelLoading, setLevelLeaveDiag
} from "../actions";

import { IVocab } from "../models/vocab";

import LevelPage from "../pages/level";

const mapStateToProps = state => {
    const {
        currentVocab, vocab, loading, stepper,
        reviewDialog, leaveDialog
    } = state.level;

    return {
        currentVocab,
        vocab,
        loading,
        stepperIndex: stepper,
        reviewDialog,
        leaveDialog,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        drawerButtonState: (state: boolean) => dispatch(setDrawerButton(state)),
        setCurrentVocab: (vocab: IVocab) => dispatch(setLevelCurrentVocab(vocab)),
        setVocab: (vocab: IVocab[]) => dispatch(setLevelVocab(vocab)),
        setLoading: (state: boolean) => dispatch(setLevelLoading(state)),
        setStepper: (index: number) => dispatch(setLevelStepper(index)),
        setReviewDialog: (state: boolean) => dispatch(setLevelReviewDiag(state)),
        setLeaveDialog: (state: boolean) => dispatch(setLevelLeaveDiag(state))
    };
};

const LevelPageContainer = connect(mapStateToProps,
    mapDispatchToProps)(LevelPage);
export default LevelPageContainer;
