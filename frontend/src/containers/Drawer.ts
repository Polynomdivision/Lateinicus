import { connect } from "react-redux";

import { setDrawer, setScorePopover } from "../actions";

import Drawer from "../components/Drawer";

const mapStateToProps = state => {
    return {
        user: state.user,
        open: state.drawer,
        authenticated: state.authenticated,
        showButton: state.drawerButton,
        scorePopoverOpen: state.scorePopoverOpen,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        setDrawer: (show: boolean) => dispatch(setDrawer(show)),
        setScorePopover: (state: boolean) => dispatch(setScorePopover(state)),
    };
};

const DrawerContainer = connect(mapStateToProps,
    mapDispatchToProps)(Drawer);
export default DrawerContainer;
