import { connect } from "react-redux";

import {
    setVocabLoading, setVocabVocab, setVocabSearchOpen,
    setVocabSearchTerm
} from "../actions";

import { IVocab } from "../models/vocab";

import VocabPage from "../pages/vocab";

const mapStateToProps = state => {
    return {
        loading: state.vocab.loading,
        vocab: state.vocab.vocab,
        searchOpen: state.vocab.searchOpen,
        searchTerm: state.vocab.searchTerm,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        setLoading: (state: boolean) => dispatch(setVocabLoading(state)),
        setVocab: (vocab: IVocab[]) => dispatch(setVocabVocab(vocab)),
        setSearchOpen: (state: boolean) => dispatch(setVocabSearchOpen(state)),
        setSearchTerm: (term: string) => dispatch(setVocabSearchTerm(term)),
    };
};

const VocabPageContainer = connect(mapStateToProps,
    mapDispatchToProps)(VocabPage);
export default VocabPageContainer;
