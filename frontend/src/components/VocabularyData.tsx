import * as React from "react";

import Typography from "@material-ui/core/Typography";

import {
    IVocab, VocabType, INomenData, IVerbData, IAdjektivData
} from "../models/vocab";

interface IProps {
    vocab: IVocab;
};

function vocabSpecificInformation(vocab: IVocab) {
    switch (vocab.type) {
        case VocabType.NOMEN:
            const nData = vocab.latin as INomenData;
            return <div>
                <Typography variant="subheading" component="p">
                    <b>Genitiv:</b> {nData.genitiv}
                </Typography>
                <Typography variant="subheading" component="p">
                    <b>Genus:</b> {nData.genus}
                </Typography>
            </div>;
        case VocabType.VERB:
            const vData = vocab.latin as IVerbData;
            return <div>
                <Typography variant="subheading" component="p">
                    <b>1. Person Präsens:</b> {vData.praesens}
                </Typography>
            </div>;
        case VocabType.ADJEKTIV:
            const aData = vocab.latin as IAdjektivData;
            return <div>
                <Typography variant="subheading" component="p">
                    <b>Endung feminin:</b> {aData.endung_f}
                </Typography>
                <Typography variant="subheading" component="p">
                    <b>Endung neutrum:</b> {aData.endung_n}
                </Typography>
            </div>;
        case VocabType.ADVERB:
            return <div />;
    }
}

export default class VocabularyData extends React.Component<IProps> {
    render() {
        const { vocab } = this.props;
        return <div>
            <Typography gutterBottom variant="headline" component="h3">
                {vocab.german.join(", ")}
            </Typography>
            {vocabSpecificInformation(vocab)}
            {
                vocab.hint ? (
                    <div style={{
                        border: "dashed",
                        borderColor: "red",
                        padding: 12,
                    }}>
                        <Typography variant="subheading" component="p">
                            <b>Tipp:</b>
                        </Typography>
                        <Typography variant="body2">
                            {vocab.hint}
                        </Typography>
                    </div>
                ) : undefined
            }
            {
                vocab.mnemonic ? (
                    <div style={{
                        border: "dashed",
                        borderColor: "#f1c40f",
                        marginTop: 12,
                        padding: 12,
                    }}>
                        <Typography variant="subheading" component="p">
                            <b>Eselsbrücke:</b>
                        </Typography>
                        <Typography variant="body2">
                            {vocab.mnemonic}
                        </Typography>
                    </div>
                ) : undefined
            }
        </div>;
    }
};
