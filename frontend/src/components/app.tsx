import * as React from "react";

import { BrowserRouter, Route, Redirect } from "react-router-dom";

import AuthRoute from "../security/AuthRoute";
import { setSessionToken, removeSessionToken, getSessionToken } from "../security/Token";

import Dashboard from "../containers/Dashboard";
import LoginPage from "../containers/LoginPage";
import LevelListPage from "../containers/LevelList";
import LevelPage from "../containers/LevelPage";
import ReviewPage from "../containers/Review";
import SummaryPage from "../containers/SummaryPage";
import WelcomePage from "../pages/intro";
import RegisterPage from "../containers/Register";
import VocabPage from "../containers/VocabPage";
import Drawer from "../containers/Drawer";

import { trackAction } from "../api/tracker";
import { makeAPICall } from "../api/call";

import { BACKEND_URL } from "../config.in";

import { TrackerEvent } from "../models/tracker";
import { IReviewMetadata, ReviewType } from "../models/review";
import { IUser } from "../models/user";
import { IResponse } from "../models/server";

interface IProps {
    authenticated: boolean;

    user: IUser;
    setAuthenticated: (status: boolean) => void;
    setDidLogin: (status: boolean) => void;
    setUser: (user: IUser) => void;
    setLastReview: (meta: IReviewMetadata) => void;
    setUserScoreDelta: (delta: number) => void;
};

export default class Application extends React.Component<IProps> {
    componentDidMount() {
        // TODO: When asking the server if our session is still valid, a spinner
        //       should be shown
        const token = getSessionToken(window);
        if (token !== undefined && !this.props.authenticated) {
            this.checkAuthStatus(token).then(user => {
                this.props.setUser(user);
                this.props.setAuthenticated(true);
            }).catch(err => {
                // The token is invalid, so we should remove it
                removeSessionToken(window);
                this.props.setAuthenticated(false);
            });
        }
    }

    checkAuthStatus = (token: string): Promise<IUser> => {
        // Track the end of a review
        trackAction(TrackerEvent.LOG_IN);

        return makeAPICall("/api/user/me", {
            token,
            method: "get",
        })
    }

    getVocab = () => makeAPICall("/api/user/vocab", {
        token: this.props.user.sessionToken,
        method: "get",
    })

    getLevels = () => makeAPICall("/api/levels", {
        token: this.props.user.sessionToken,
        method: "get",
    });

    getLastReview = () => makeAPICall("/api/user/lastReview", {
        token: this.props.user.sessionToken,
        method: "get",
    });

    // TODO: Type?
    setLastReview = (meta: IReviewMetadata, sm2: any, delta: number) => {
        // Update the state
        this.props.setLastReview(meta);
        this.props.setUserScoreDelta(delta);

        // Tell the server about the last review
        makeAPICall("/api/user/lastReview", {
            token: this.props.user.sessionToken,
            body: {
                meta,
                sm2,
                delta,
            },
            method: "post",
        });

        // Track the end of a review
        trackAction(TrackerEvent.FINISH_LEARNING);
    }

    getReviewQueue = () => makeAPICall("/api/user/queue", {
        token: this.props.user.sessionToken,
        method: "get",
    });

    getNextLevel = () => makeAPICall("/api/user/nextLevel", {
        token: this.props.user.sessionToken,
        method: "get",
    });

    getLevelVocab = (id: number) => makeAPICall(`/api/level/${id}/vocab`, {
        token: this.props.user.sessionToken,
        method: "get",
    });

    // NOTE: This is not a promise, as we do not care about any response
    //       being sent, since we don't need to update any client-side
    //       state.
    introDontShowAgain = () => makeAPICall("/api/user/showWelcome", {
        token: this.props.user.sessionToken,
        body: {
            state: false,
        },
        method: "post",
    });

    getDashboard = () => makeAPICall("/api/user/dashboard", {
        token: this.props.user.sessionToken,
        method: "get",
    });

    updateDoneLevels = (id: string) => makeAPICall(`/api/user/level/${id}`, {
        token: this.props.user.sessionToken,
        method: "post",
    });

    login = (username: string, password: string): Promise<IUser | IResponse> => {
        // Track the login
        trackAction(TrackerEvent.LOG_IN);

        return new Promise((res, rej) => {
            fetch(`${BACKEND_URL}/api/login`, {
                method: "POST",
                headers: new Headers({
                    "Content-Type": "application/json",
                }),
                body: JSON.stringify({
                    // NOTE: We will force HTTPS, so this should not be a
                    // problem
                    username,
                    password,
                }),
            }).then(data => data.json(), err => {
                // The fetch failed
                rej(err);
            }).then((resp: IResponse) => {
                if (resp.error === "200") {
                    // Successful login
                    this.props.setUser(resp.data);
                    this.props.setDidLogin(true);
                    setSessionToken(window, resp.data.sessionToken);
                    this.props.setAuthenticated(true);

                    res(resp.data);
                } else {
                    rej(resp);
                }
            });
        });
    }

    logout = () => {
        // Track the logout
        trackAction(TrackerEvent.LOG_OUT);

        // NOTE: No promise, since we don't care about the result
        fetch(`${BACKEND_URL}/api/user/logout`, {
            headers: new Headers({
                "Content-Type": "application/json",
                "Token": this.props.user.sessionToken,
            }),
            method: "GET",
        });

        // Remove the session locally
        removeSessionToken(window);
        this.props.setAuthenticated(false);
    }

    // Checks whether the user is logged in
    isAuthenticated = () => {
        return this.props.authenticated;
    }

    render() {
        // TODO: Show a spinner before mounting the routes, so that we can
        //       check if were authenticated before doing any requests
        return <BrowserRouter
            basename="/app/">
            <div className="flex" >
                <Drawer logout={this.logout} />
                <div>
                    <Route exact path="/" component={() => <Redirect to="/login" />} />
                    <Route exact path="/login" component={() => {
                        return <LoginPage login={this.login} />
                    }} />
                    <AuthRoute
                        isAuth={this.isAuthenticated}
                        path="/dashboard"
                        component={() => {
                            return <Dashboard
                                getDashboard={this.getDashboard} />
                        }} />
                    <AuthRoute
                        isAuth={this.isAuthenticated}
                        path="/welcome"
                        component={() => {
                            return <WelcomePage
                                dontShowAgain={this.introDontShowAgain} />
                        }} />
                    <AuthRoute
                        isAuth={this.isAuthenticated}
                        path="/levelList"
                        component={() => <LevelListPage
                            getLevels={this.getLevels} />} />
                    {/*We cannot use AuthRoute here, because match is undefined otherwise*/}
                    <Route
                        path="/level/:id"
                        component={({ match }) => {
                            if (this.isAuthenticated()) {
                                return <LevelPage
                                    id={match.params.id}
                                    levelVocab={this.getLevelVocab}
                                    setLastReview={this.setLastReview} />;
                            } else {
                                return <Redirect to="/login" />;
                            }
                        }} />
                    <Route
                        path="/register"
                        component={() => <RegisterPage />} />
                    <Route
                        path="/review/level/:id"
                        component={({ match }) => {
                            if (this.isAuthenticated()) {
                                return <ReviewPage
                                    reviewType={ReviewType.LEVEL}
                                    updateDoneLevels={this.updateDoneLevels}
                                    levelId={match.params.id}
                                    vocabByLevel={this.getLevelVocab}
                                    setLastReview={this.setLastReview} />;
                            } else {
                                return <Redirect to="/login" />;
                            }
                        }} />
                    <AuthRoute
                        isAuth={this.isAuthenticated}
                        path="/review/queue"
                        component={() => {
                            return <ReviewPage
                                reviewType={ReviewType.QUEUE}
                                vocabByQueue={this.getReviewQueue}
                                setLastReview={this.setLastReview} />;
                        }} />
                    <AuthRoute
                        isAuth={this.isAuthenticated}
                        path="/review/summary"
                        component={() => {
                            return <SummaryPage />
                        }} />
                    <AuthRoute
                        isAuth={this.isAuthenticated}
                        path="/vocab"
                        component={() => {
                            return <VocabPage
                                getVocab={this.getVocab} />;
                        }} />
                </div>
            </div >
        </BrowserRouter >;
    }
};
