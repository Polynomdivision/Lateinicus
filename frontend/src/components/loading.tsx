import * as React from "react";

import CircularProgress from "@material-ui/core/CircularProgress";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

export default class Loader extends React.Component<{}> {
    render() {
        return <div className="center">
            <Paper className="paper">
                <Grid container direction="column" spacing={8}>
                    <CircularProgress />
                </Grid>
            </Paper>
        </div>;
    }
}
