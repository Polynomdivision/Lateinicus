import * as React from "react";

import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

import { IReviewMetadata } from "../models/review";

interface IProps {
    reviewMeta: IReviewMetadata;
}

export default class SummaryTable extends React.Component<IProps> {
    render() {
        const { reviewMeta } = this.props;

        return <Table>
            <TableHead>
                <TableRow>
                    <TableCell>Antworten</TableCell>
                    <TableCell numeric>Anzahl</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                <TableRow>
                    <TableCell>Korrekt</TableCell>
                    <TableCell numeric>{reviewMeta.correct}</TableCell>
                </TableRow>
                <TableRow>
                    <TableCell>Falsch</TableCell>
                    <TableCell numeric>{reviewMeta.wrong}</TableCell>
                </TableRow>
            </TableBody>
        </Table>;
    }
}
