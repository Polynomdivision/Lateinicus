import * as React from "react";

import { Link } from "react-router-dom";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import Popover from "@material-ui/core/Popover";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import Avatar from "@material-ui/core/Avatar";
import MenuIcon from "@material-ui/icons/Menu";
/* import SettingsIcon from "@material-ui/icons/Settings"; */
import PersonIcon from "@material-ui/icons/Person";
import ListIcon from "@material-ui/icons/List";
import InfoIcon from "@material-ui/icons/Info";
import HomeIcon from "@material-ui/icons/Home";
import BookIcon from "@material-ui/icons/Book";
import ViewWeekIcon from "@material-ui/icons/ViewWeek";

import { IUser, userScoreToLevel } from "../models/user";

interface IProps {
    logout: () => void;

    user: IUser;
    open: boolean;

    scorePopoverOpen: boolean;
    showButton: boolean;
    authenticated: boolean;
    setDrawer: (state: boolean) => void;
    setScorePopover: (state: boolean) => void;
};

export default class Drawer extends React.Component<IProps> {
    private scoreBtnRef: HTMLButtonElement = undefined;

    openDrawer = () => {
        this.props.setDrawer(true);
    }
    closeDrawer = () => {
        this.props.setDrawer(false);
    }

    toggleScorePopover = () => {
        this.props.setScorePopover(!this.props.scorePopoverOpen);
    }
    closeScorePopover = () => {
        this.props.setScorePopover(false);
    }

    render() {
        const { score } = this.props.user;
        const level = userScoreToLevel(score);

        // Determine whether we hit the maximum level
        const maxLevel = score >= level.levelCap;
        const scoreStr = maxLevel ? (
            `${score}`
        ) : (
                `${score} / ${level.levelCap}`
            );

        return (
            <div>
                <AppBar position="static">
                    <Toolbar>
                        {
                            (this.props.authenticated && this.props.showButton) ? (
                                <IconButton
                                    color="inherit"
                                    onClick={this.openDrawer}>
                                    <MenuIcon />
                                </IconButton>
                            ) : undefined
                        }
                        <Typography className="flex" variant="title" color="inherit">
                            Lateinicus
                        </Typography>
                        {
                            this.props.authenticated ? (
                                <Button
                                    color="inherit"
                                    buttonRef={node => this.scoreBtnRef = node}
                                    onClick={this.toggleScorePopover}>
                                    {scoreStr}
                                </Button>
                            ) : undefined
                        }
                        <Popover
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'right',
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            anchorEl={this.scoreBtnRef}
                            open={this.props.scorePopoverOpen}
                            onClose={this.closeScorePopover}>
                            <div className="content">
                                <Typography variant="headline">Du bist: {level.name}</Typography>
                                {
                                    maxLevel ? (
                                        <Typography variant="subheading">
                                            Veni Vidi Vici...
                                        </Typography>
                                    ) : (
                                            <Typography variant="subheading">
                                                Dir fehlen noch <b>{level.levelCap - this.props.user.score}</b> Erfahrungspunkte bis zum nächsten Level
                                        </Typography>
                                        )
                                }
                            </div>
                        </Popover>
                    </Toolbar>
                </AppBar>
                <SwipeableDrawer
                    anchor="left"
                    open={this.props.open}
                    onClose={this.closeDrawer}
                    onOpen={this.openDrawer}>
                    <List component="nav">
                        <ListItem>
                            <ListItemAvatar>
                                <Avatar>
                                    <PersonIcon />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={this.props.user.username} />
                        </ListItem>

                        <Divider />
                        {/*
                        <ListItem button>
                            <ListItemIcon>
                                <PersonIcon />
                            </ListItemIcon>
                            <ListItemText primary="Profil" />
                        </ListItem>
                        <ListItem button>
                            <ListItemIcon>
                                <SettingsIcon />
                            </ListItemIcon>
                            <ListItemText primary="Einstellungen" />
                        </ListItem>

                        <Divider />*/}

                        <ListItem
                            component={Link}
                            to="/dashboard"
                            onClick={this.closeDrawer}
                            button>
                            <ListItemIcon>
                                <HomeIcon />
                            </ListItemIcon>
                            <ListItemText primary="Dashboard" />
                        </ListItem>
                        <ListItem
                            component={Link}
                            to="/review/queue"
                            onClick={this.closeDrawer}
                            button>
                            <ListItemIcon>
                                <BookIcon />
                            </ListItemIcon>
                            <ListItemText>
                                Vokabeln üben
                            </ListItemText>
                        </ListItem>
                        <ListItem
                            component={Link}
                            to="/vocab"
                            onClick={this.closeDrawer}
                            button>
                            <ListItemIcon>
                                <ListIcon />
                            </ListItemIcon>
                            <ListItemText primary="Gelernte Vokabeln" />
                        </ListItem>
                        <ListItem
                            component={Link}
                            to="/levelList"
                            onClick={this.closeDrawer}
                            button>
                            <ListItemIcon>
                                <ViewWeekIcon />
                            </ListItemIcon>
                            <ListItemText>
                                Levelübersicht
                            </ListItemText>
                        </ListItem>

                        <Divider />

                        <ListItem button onClick={() => {
                            this.closeDrawer();
                            this.props.logout();
                        }}>
                            <ListItemText>
                                Abmelden
                            </ListItemText>
                        </ListItem>

                        <Divider />

                        <ListItem button onClick={() => window.location = "https://gitlab.com/Polynomdivision/Lateinicus/tree/master"}>
                            <ListItemIcon>
                                <InfoIcon />
                            </ListItemIcon>
                            <ListItemText primary="Über" />
                        </ListItem>
                    </List>
                </SwipeableDrawer>
            </div >
        );
    }
};
