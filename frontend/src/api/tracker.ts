import { TrackerEvent } from "../models/tracker";

import { BACKEND_URL } from "../config.in";

// Sends a tracker event to the remote API
// @event: The kind of event to track
export function trackAction(event: TrackerEvent) {
    // Get the tracker ID
    const sid = window.sessionStorage.getItem("tracker_session");

    fetch(`${BACKEND_URL}/api/tracker`, {
        headers: new Headers({
            "Content-Type": "application/json",
        }),
        method: "POST",
        body: JSON.stringify({
            session: sid,
            event,
        }),
    });
};
