import { BACKEND_URL } from "../config.in";

interface IAPIOptions {
    token?: string;
    body?: any;
    method: "post" | "get";
};
export function makeAPICall<T>(endpoint: string, options: IAPIOptions): Promise<T> {
    const { token, body, method } = options;
    const headers = token !== "" ? ({
        "Content-Type": "application/json",
        "Token": token,
    }) : ({
        "Content-Type": "application/json",
    });

    return new Promise((res, rej) => {
        fetch(`${BACKEND_URL}${endpoint}`, {
            // WHUT
            headers: new Headers(headers),
            body: body !== {} ? JSON.stringify(body) : "",
            method,
        })
            .then(resp => resp.json(), err => rej(err))
            .then(data => {
                if (data.error === "200") {
                    res(data.data);
                } else {
                    rej(data);
                }
            });
    });
}
