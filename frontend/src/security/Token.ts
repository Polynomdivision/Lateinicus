export function setSessionToken(window: Window, token: string) {
    window.localStorage.setItem("sessionToken", token);
};

export function getSessionToken(window: Window) {
    return window.localStorage.getItem("sessionToken");
}

export function removeSessionToken(window: Window) {
    window.localStorage.removeItem("sessionToken");
}
