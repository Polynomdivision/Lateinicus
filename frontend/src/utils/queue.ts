export class Queue<T> {
    private elements: T[] = [];

    enqueue = (element: T) => {
        this.elements.push(element);
    }

    dequeue = (): T => {
        return this.elements.shift();
    }

    size = (): number => {
        return this.elements.length;
    }
};
