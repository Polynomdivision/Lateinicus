export const DAY_IN_MILLI = 24 * 60 * 60 * 1000;

export function daysToMilli(n: number): number {
    return DAY_IN_MILLI * n;
};

export function dayInNDays(n: number): number {
    return Date.now() * daysToMilli(n);
}
