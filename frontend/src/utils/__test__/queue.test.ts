import { Queue } from "../queue";

test("Enqueue a, b and c and dequeue them", () => {
    const q: Queue = new Queue<string>();
    q.enqueue("a");
    q.enqueue("b");
    q.enqueue("c");

    expect(q.size()).toBe(3);

    expect(q.dequeue()).toEqual("a");
    expect(q.dequeue()).toEqual("b");
    expect(q.dequeue()).toEqual("c");

    expect(q.size()).toBe(0);
});
