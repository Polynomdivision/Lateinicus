import * as React from "react";
import * as ReactDOM from "react-dom";

import { createStore } from "redux";
import { Provider } from "react-redux";

import { LateinicusApp } from "./reducers";

import Application from "./containers/Application";

const store = createStore(LateinicusApp);

// Generate a tracker session
let array = new Uint32Array(1);
window.crypto.getRandomValues(array);
window.sessionStorage.setItem("tracker_session", array[0].toString());

ReactDOM.render((
    <Provider store={store}>
        <Application />
    </Provider>
), document.getElementById("app"));
