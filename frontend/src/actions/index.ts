import { IVocab, IReviewCard } from "../models/vocab";
import { IUser } from "../models/user";
import { ILevel } from "../models/level";
import { IReviewMetadata } from "../models/review";
import { TopTen } from "../models/learner";

export const SET_DRAWER = "SET_DRAWER";
export function setDrawer(state: boolean) {
    return {
        type: SET_DRAWER,
        show: state,
    };
};

export const SET_DRAWER_BUTTON = "SET_DRAWER_BUTTON";
export function setDrawerButton(state: boolean) {
    return {
        type: SET_DRAWER_BUTTON,
        show: state,
    };
};

export const LOGIN_SET_SNACKBAR = "LOGIN_SET_SNACKBAR";
export function setLoginSnackbar(visibility: boolean, msg: string = "") {
    return {
        type: LOGIN_SET_SNACKBAR,
        show: visibility,
        msg,
    };
};

export const LOGIN_SET_LOADING = "LOGIN_SET_LOADING";
export function setLoginLoading(visibility: boolean) {
    return {
        type: LOGIN_SET_LOADING,
        show: visibility,
    };
};

export const SET_AUTHENTICATED = "SET_AUTHENTICATED";
export function setAuthenticated(state: boolean) {
    return {
        type: SET_AUTHENTICATED,
        status: state,
    };
}

export const SET_USER = "SET_USER";
export function setUser(user: IUser) {
    return {
        type: SET_USER,
        user,
    };
}

export const LEVEL_SET_CUR_VOCAB = "LEVEL_SET_CUR_VOCAB";
export function setLevelCurrentVocab(vocab: IVocab) {
    return {
        type: LEVEL_SET_CUR_VOCAB,
        vocab,
    };
};

export const LEVEL_SET_VOCAB = "LEVEL_SET_VOCAB";
export function setLevelVocab(vocab: IVocab[]) {
    return {
        type: LEVEL_SET_VOCAB,
        vocab,
    };
};

export const LEVEL_SET_LOADING = "LEVEL_SET_LOADING";
export function setLevelLoading(state: boolean) {
    return {
        type: LEVEL_SET_LOADING,
        state,
    };
};

export const LEVEL_SET_STEPPER = "LEVEL_SET_STEPPER";
export function setLevelStepper(index: number) {
    return {
        type: LEVEL_SET_STEPPER,
        index,
    };
};

export const LEVEL_SET_REVIEW_DIAG = "LEVEL_SET_REVIEW_DIAG";
export function setLevelReviewDiag(state: boolean) {
    return {
        type: LEVEL_SET_REVIEW_DIAG,
        state,
    };
};

export const LEVEL_SET_LEAVE_DIAG = "LEVEL_SET_LEAVE_DIAG";
export function setLevelLeaveDiag(state: boolean) {
    return {
        type: LEVEL_SET_LEAVE_DIAG,
        state,
    };
};

export const SET_LEVELS = "SET_LEVELS";
export function setLevels(levels: ILevel[]) {
    return {
        type: SET_LEVELS,
        levels,
    };
};

export const REVIEW_SET_POPOVER = "REVIEW_SET_POPOVER";
export function setReviewPopover(state: boolean, text: string, color: string, textColor: string) {
    return {
        type: REVIEW_SET_POPOVER,
        state,
        text,
        color,
        textColor,
    };
};

export const REVIEW_SET_MODAL = "REVIEW_SET_MODAL";
export function setReviewModal(state: boolean) {
    return {
        type: REVIEW_SET_MODAL,
        state,
    };
};

export const REVIEW_SET_LOADING = "REVIEW_SET_LOADING";
export function setReviewLoading(state: boolean) {
    return {
        type: REVIEW_SET_LOADING,
        state,
    };
};

export const SET_LAST_REVIEW = "SET_LAST_REVIEW";
export function setLastReview(metadata: IReviewMetadata) {
    return {
        type: SET_LAST_REVIEW,
        metadata,
    };
};

export const SET_USER_SCORE_DELTA = "SET_USER_SCORE_DELTA";
export function setUserScoreDelta(delta: number) {
    return {
        type: SET_USER_SCORE_DELTA,
        delta,
    };
};

export const SET_REVIEW = "SET_REVIEW";
export function setReview(current: IReviewCard, meta: IReviewMetadata) {
    return {
        type: SET_REVIEW,
        current,
        meta,
    };
};

export const LEVELLIST_SET_LOADING = "LEVELLIST_SET_LOADING";
export function setLevelListLoading(state: boolean) {
    return {
        type: LEVELLIST_SET_LOADING,
        state,
    };
};

export const SET_SCORE_POPOVER = "SET_SCORE_POPOVER";
export function setScorePopover(state: boolean) {
    return {
        type: SET_SCORE_POPOVER,
        state,
    };
};

export const SET_NEXT_LEVEL = "SET_NEXT_LEVEL";
export function setNextLevel(level: ILevel) {
    return {
        type: SET_NEXT_LEVEL,
        level,
    };
};

export const SET_TOP_TEN = "SET_TOP_TEN";
export function setTopTen(topTen: TopTen[]) {
    return {
        type: SET_TOP_TEN,
        topTen,
    };
};

export const SET_DID_LOGIN = "SET_DID_LOGIN";
export function setDidLogin(state: boolean) {
    return {
        type: SET_DID_LOGIN,
        state,
    };
};

export const REVIEW_SET_DIALOG = "REVIEW_SET_DIALOG";
export function setReviewDialog(state: boolean) {
    return {
        type: REVIEW_SET_DIALOG,
        state,
    };
};

export const DASHBOARD_SET_LOADING = "DASHBOARD_SET_LOADING";
export function setDashboardLoading(state: boolean) {
    return {
        type: DASHBOARD_SET_LOADING,
        state,
    };
};

export const LEVELLIST_SET_SNACKBAR = "LEVELLIST_SET_SNACKBAR";
export function setLevelListSnackbar(state: boolean) {
    return {
        type: LEVELLIST_SET_SNACKBAR,
        state,
    };
};

export const REVIEW_SET_HELP = "REVIEW_SET_HELP";
export function setReviewHelp(state: boolean) {
    return {
        type: REVIEW_SET_HELP,
        state,
    };
};

export const REGISTER_SET_SNACKBAR = "REGISTER_SET_SNACKBAR";
export function setRegisterSnackbar(state: boolean, msg: string) {
    return {
        type: REGISTER_SET_SNACKBAR,
        state,
        msg,
    };
};

export const REGISTER_SET_LOADING = "REGISTER_SET_LOADING";
export function setRegisterLoading(state: boolean) {
    return {
        type: REGISTER_SET_LOADING,
        state,
    };
};

export const VOCAB_SET_LOADING = "VOCAB_SET_LOADING";
export function setVocabLoading(state: boolean) {
    return {
        type: VOCAB_SET_LOADING,
        state,
    };
};

export const VOCAB_SET_VOCAB = "VOCAB_SET_VOCAB";
export function setVocabVocab(vocab: IVocab[]) {
    return {
        type: VOCAB_SET_VOCAB,
        vocab,
    };
};

export const VOCAB_SET_SEARCH_OPEN = "VOCAB_SET_SEARCH_OPEN";
export function setVocabSearchOpen(state: boolean) {
    return {
        type: VOCAB_SET_SEARCH_OPEN,
        state,
    };
};

export const VOCAB_SET_SEARCH_TERM = "VOCAB_SET_SEARCH_TERM";
export function setVocabSearchTerm(term: string) {
    return {
        type: VOCAB_SET_SEARCH_TERM,
        term,
    };
};
